<?php

namespace FleetControl;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\ModuleManager\ModuleManager;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function init(ModuleManager $moduleManager) {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        
        $sharedEvents->attach("Zend\Mvc\Controller\AbstractActionController", 
                MvcEvent::EVENT_DISPATCH,
                array($this,'mvcPreDispatch'),100);
    }
    
    public function mvcPreDispatch($e) {
        $auth = new AuthenticationService;
        $auth->setStorage(new SessionStorage("FleetControl"));
        
        $controller = $e->getTarget();
        $matchedRoute = $controller->getEvent()->getRouteMatch()->getMatchedRouteName();

        if ($matchedRoute == "app-router")
            return $controller->redirect()->toRoute("app-home");

        if ($matchedRoute != "app-password-recover" ) {
            if(!$auth->hasIdentity() and ($matchedRoute == "app-router" OR $matchedRoute == "app-router/default" OR $matchedRoute == "app-router/paginator")) {
                return $controller->redirect()->toRoute("app-auth");
            }
        }
    }
    
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'FleetControl\Permissions\Acl' => 'FleetControl\Factory\Permissions\AclFactory',
                'FleetControl\Auth\Adapter' => 'FleetControl\Factory\Auth\AdapterFactory',
                'FleetControl\Form\User' => 'FleetControl\Factory\Form\UserFormFactory',
                'FleetControl\Form\Role' => 'FleetControl\Factory\Form\RoleFormFactory',
                'FleetControl\Form\Resource' => 'FleetControl\Factory\Form\ResourceFormFactory',
                'FleetControl\Form\Privilege' => 'FleetControl\Factory\Form\PrivilegeFormFactory',
                'FleetControl\Service\User' => 'FleetControl\Factory\Service\UserServiceFactory',
                'FleetControl\Service\Role' => 'FleetControl\Factory\Service\RoleServiceFactory',
                'FleetControl\Service\Resource' => 'FleetControl\Factory\Service\ResourceServiceFactory',
                'FleetControl\Service\Privilege' => 'FleetControl\Factory\Service\PrivilegeServiceFactory',
            ]
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                'FleetControl\Controller\User' => 'FleetControl\Factory\Controller\UserControllerFactory',
                'FleetControl\Controller\Role' => 'FleetControl\Factory\Controller\RoleControllerFactory',
                'FleetControl\Controller\Resource' => 'FleetControl\Factory\Controller\ResourceControllerFactory',
                'FleetControl\Controller\Privilege' => 'FleetControl\Factory\Controller\PrivilegeControllerFactory',
            ],
            'invokables' => [
                'FleetControl\Controller\Index' => 'FleetControl\Controller\IndexController',
                'FleetControl\Controller\Auth' => 'FleetControl\Controller\AuthController',
            ]
        ];
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig() 
    {
        return array(
            'invokables' => array(
                'UserIdentity'  => 'FleetControl\View\Helper\UserIdentity',
                'ProfileName'   => 'FleetControl\View\Helper\ProfileName',
                'StatusName'    => 'FleetControl\View\Helper\StatusName',
                'AclPermission' => 'FleetControl\View\Helper\AclPermission',
                'CompleteUserIdentity' => 'FleetControl\View\Helper\CompleteUserIdentity',
                'GetControllerRoute' => 'FleetControl\View\Helper\GetControllerRoute',
            ),
        );
    }
}