<?php

namespace FleetControl;

return [
	'router' => [
        'routes' => [
            'app-home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/app/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'FleetControl\Controller',
                        'controller' => 'Index',
                        'action' => 'index'
                    )
                )
            ),
            'app-auth' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/app/auth/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'FleetControl\Controller',
                        'controller' => 'Auth',
                        'action' => 'index'
                    )
                )
            ),
            'app-logout' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/app/auth/logout',
                    'defaults' => array(
                        '__NAMESPACE__' => 'FleetControl\Controller',
                        'controller' => 'Auth',
                        'action' => 'logout'
                    )
                )
            ),
            'app-password-recover' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/auth/validate-email',
                    'defaults' => array(
                        '__NAMESPACE__' => 'FleetControl\Controller',
                        'controller' => 'Auth',
                        'action' => 'password-recover'
                    )
                )
            ),
            'app-router' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'FleetControl\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => 'app/[:controller[/:action[/:id]]]',
                            'constraints' => [
                                'controller' => '[a-zA-z][a-zA-A-Z0-9_-]*',
                                'action' => '[a-zA-z][a-zA-A-Z0-9_-]*',
                                'id' => '\d+'
                            ],
                            'defaults' => [
                                '__NAMESPACE__' => 'FleetControl\Controller',
                                'controller' => 'index',
                            ]
                        ]
                    ],
                    'paginator' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => 'app/[:controller[/page/:page]]',
                            'constraints' => [
                                'controller' => '[a-zA-z][a-zA-A-Z0-9_-]*',
                                'action' => '[a-zA-z][a-zA-A-Z0-9_-]*',
                                'page' => '\d+'
                            ],
                            'defaults' => [
                                '__NAMESPACE__' => 'FleetControl\Controller',
                                'controller' => 'index',
                            ]
                        ]
                    ],
                ],
            ],
        ],
    ],
	'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ],
            ],
        ],
    ],
];