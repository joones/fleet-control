<?php

namespace FleetControl\Service;

use Core\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;
use FleetControl\Event\AclEvent;

class User extends AbstractService 
{
    public function __construct(\Doctrine\ORM\EntityManager $em, $entity) 
    {
        parent::__construct($em);
        $this->entity = $entity;
    }

    public function insert(array $data) {
        $entity = new $this->entity($data);

        $role = $this->em->getReference("FleetControl\Entity\Role", $data['role']);
        $entity->setRole($role);

        $entity->setDeleted('0');

        $this->em->persist($entity);
        $this->em->flush();

        $event = new AclEvent($this->em);
        $event->register($entity->getId(), $role->getId()); 

        return $entity;
    }
    
    public function update(array $data) {
        $entity = $this->em->getReference($this->entity, $data['id']);
        
        (new Hydrator\ClassMethods())->hydrate($data, $entity);
        $entity->setUpdated(new \DateTime("now"));
        
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
        
    }
}