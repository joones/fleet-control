<?php 

namespace FleetControl\Service;

use Core\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;

class Resource extends AbstractService
{
	public function __construct(\Doctrine\ORM\EntityManager $em, $entity)
	{
		parent::__construct($em);
		$this->entity = $entity;
	}
}