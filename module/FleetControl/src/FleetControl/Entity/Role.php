<?php 

namespace FleetControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="FleetControl\Repository\RoleRepository")
 */
class Role
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @ORM\OneToOne(targetEntity="FleetControl\Entity\Role")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
	 */
	protected $parent;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $name;

	/**
	 * @ORM\Column(type="boolean", name="is_admin")
	 * @var string
	 */
	protected $admin;

	/**
	 * @ORM\Column(type="datetime", name="created_at")
	 * @var string
	 */
	protected $created;

	/**
	 * @ORM\Column(type="datetime", name="updated_at")
	 * @var string
	 */
	protected $updated;

	/**
	 * @ORM\Column(type="boolean", name="deleted")
	 * @var string
	 */
	protected $deleted;

	public function __construct($options = [])
	{
		(new Hydrator\ClassMethods)->hydrate($options, $this);
		$this->created = new \DateTime("now");
		$this->updated = new \DateTime("now");
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getParent()
	{
		return $this->parent;
	}

	public function setParent($parent)
	{
		$this->parent = $parent;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	public function getAdmin()
	{
		return $this->admin;
	}

	public function setAdmin($admin)
	{
		$this->admin = $admin;
		return $this;
	}

	function getCreated() {
        return $this->created;
    }
    
    function setCreated(\DateTime $created) {
        $this->created = $created;
        return $this;
    }
    
  
    function getUpdated() {
        return $this->updated;
    }

    function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
        return $this;
    }

    public function getDeleted()
	{
		return $this->deleted;
	}

	public function setDeleted($deleted)
	{
		$this->deleted = $deleted;
		return $this;
	}

    public function __toString()
    {
        return $this->name;
    }

    
    public function toArray() {
        if (isset($this->parent)) {
        	$parent = $this->parent->getId();
        } else {
        	$parent = false;
        }

        return [
        	'id' => $this->id,
        	'name' => $this->name,
        	'admin' => $this->admin,
        	'parent' => $parent
        ];
    }
}	