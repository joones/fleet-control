<?php 

namespace FleetControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="privilege")
 * @ORM\Entity(repositoryClass="FleetControl\Repository\PrivilegeRepository")
 */
class Privilege
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;


	/**
	 * @ORM\OneToOne(targetEntity="FleetControl\Entity\Role")
	 * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
	 */
	protected $role;

	/**
	 * @ORM\OneToOne(targetEntity="FleetControl\Entity\Resource")
	 * @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
	 */
	protected $resource;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $name;

	/**
	 * @ORM\Column(type="datetime", name="created_at")
	 * @var string
	 */
	protected $created;

	/**
	 * @ORM\Column(type="datetime", name="updated_at")
	 * @var string
	 */
	protected $updated;

	/**
	 * @ORM\Column(type="boolean", name="deleted")
	 * @var string
	 */
	protected $deleted;

	public function __construct($options = [])
	{
		(new Hydrator\ClassMethods)->hydrate($options, $this);
		$this->created = new \DateTime("now");
		$this->updated = new \DateTime("now");
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getRole()
	{
		return $this->role;
	}

	public function setRole($role)
	{
		$this->role = $role;
		return $this;
	}

	public function getResource()
	{
		return $this->resource;
	}

	public function setResource($resource)
	{
		$this->resource = $resource;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	function getCreated() {
        return $this->created;
    }
    
    function setCreated(\DateTime $created) {
        $this->created = $created;
        return $this;
    }
  
    function getUpdated() {
        return $this->updated;
    }

    function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
        return $this;
    }

    public function getDeleted()
	{
		return $this->deleted;
	}

	public function setDeleted($deleted)
	{
		$this->deleted = $deleted;
		return $this;
	}

    public function toArray() {
        return [
        	'id' => $this->id,
        	'name' => $this->name,
        	'role' => $this->role->getId(),
        	'resource' => $this->resource->getId()
        ];
    }
}	