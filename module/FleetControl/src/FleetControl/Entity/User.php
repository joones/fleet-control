<?php

namespace FleetControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="FleetControl\Repository\UserRepository")
 */
class User {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="FleetControl\Entity\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    private $role;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=32, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="status")
     */
    private $status;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $created;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="deleted", type="string")
     */
    private $deleted;
    
    public function __construct(array $options = array()) {
        (new Hydrator\ClassMethods())->hydrate($options, $this);
        
        $this->created = new \DateTime("now");
        $this->updated = new \DateTime("now");
    }

    function getId() {
        return $this->id;
    }
    
    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function getRole() {
        return $this->role;
    }
    
    function setRole($role) {
        $this->role = $role;
        return $this;
    }
    
    function getName() {
        return $this->name;
    }
    
    function setName($name) {
        $this->name = $name;
        return $this;
    }

    function getEmail() {
        return $this->email;
    }
    
    function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    function getPassword() {
        return $this->password;
    }
    function setPassword($password) {
        $this->password = md5($password);
        return $this;
    }

    function getStatus() {
        return $this->status;
    }
    
    function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    function getCreated() {
        return $this->created;
    }
    
    function setCreated(\DateTime $created) {
        $this->created = $created;
        return $this;
    }
    
  
    function getUpdated() {
        return $this->updated;
    }

    function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
        return $this;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
        return $this;
    }
    
    public function toArray() {
        return (new Hydrator\ClassMethods())->extract($this);
    }
}