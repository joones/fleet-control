<?php 

namespace FleetControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="acl")
 * @ORM\Entity(repositoryClass="FleetControl\Repository\AclRepository")
 */
class Acl
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;


	/**
	 * @ORM\OneToOne(targetEntity="FleetControl\Entity\Privilege")
	 * @ORM\JoinColumn(name="privilege_id", referencedColumnName="id")
	 */
	protected $privilege;

	/**
	 * @ORM\OneToOne(targetEntity="FleetControl\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 */
	protected $user;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $status;

	public function __construct($options = [])
	{
		(new Hydrator\ClassMethods)->hydrate($options, $this);
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getPrivilege()
	{
		return $this->privilege;
	}

	public function setPrivilege($privilege)
	{
		$this->privilege = $privilege;
		return $this;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

    public function toArray() {
        return [
        	'id' => $this->id,
        	'privilege' => $this->privilege->getId(),
        	'user' => $this->user->getId(),
        	'status' => $this->status,
        ];
    }
}	