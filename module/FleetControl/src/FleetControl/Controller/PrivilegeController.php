<?php 

namespace FleetControl\Controller;

use Core\Controller\CrudController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Doctrine\ORM\EntityManager;
use FleetControl\Event\SecurityEvent;

class PrivilegeController extends CrudController
{
    protected $em; 
    protected $securityEvent;

	public function __construct(EntityManager $em, $entity, $form, $service, $controller, $route) {
        $this->em         = $em;
        $this->entity     = $entity;
        $this->form       = $form;
        $this->service    = $service;
        $this->controller = $controller;
        $this->route      = $route;

        $this->securityEvent = new SecurityEvent($this->em);
    }

    public function indexAction() {
        $list = $this->em
            ->getRepository($this->entity)
            ->findPrivileges();

        $page = $this->params()->fromRoute('page');

        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
            ->setDefaultItemCountPerPage(12);

        return new ViewModel(array('data' => $paginator, 'page' => $page));
    }

    public function newAction() {
        $msg = null;
        $form = $this->form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $role_exists = $this->em->getRepository('FleetControl\Entity\Role')
                    ->roleExists($data['role']);

                if ($data['role'] == "0" || !$role_exists) {
                    $msg = ['error' => 'Este perfil não existe.'];
                    return new ViewModel(['form' => $form, 'msg' => $msg]);
                }

                $resource_exists = $this->em->getRepository('FleetControl\Entity\Resource')
                    ->exists($data['resource']);

                if ($data['resource'] == "0" || !$resource_exists) {
                    $msg = ['error' => 'Este recurso não existe.'];
                    return new ViewModel(['form' => $form, 'msg' => $msg]);
                }  

                if (!empty($data['name'])) {
                    $name_exists = $this->em->getRepository($this->entity)
                    ->nameExists($data['name'], $data['role'], $data['resource']);

                    if ($name_exists) {
                        $msg = ['error' => 'Este privilégio já existe.'];
                        return new ViewModel(['form' => $form, 'msg' => $msg]);
                    }
                } else {
                    $msg = ['error' => 'Informe um nome para o privilégio.'];
                    return new ViewModel(['form' => $form, 'msg' => $msg]);
                }  

                if (
                    !$this->securityEvent->securityVerify($data['role']) ||
                    !$this->securityEvent->securityVerify($data['resource']) ||
                    !$this->securityEvent->securityVerify($data['name']) 
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->insert($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel(array('form' => $form));
    }

    public function editAction() {
        $msg = null;
        $form = $this->form;
        $request = $this->getRequest();
        
        $repository = $this->em->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));
        
        if ($this->params()->fromRoute('id', 0)) {
            $form->setData($entity->toArray());
        }
        
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $role_exists = $this->em->getRepository('FleetControl\Entity\Role')
                    ->roleExists($data['role']);

                if ($data['role'] == "0" || !$role_exists) {
                    $msg = ['error' => 'Este perfil não existe.'];
                    return new ViewModel([
                        'form' => $form, 
                        'msg' => $msg,
                        'entity' => $entity
                    ]);
                }

                $resource_exists = $this->em->getRepository('FleetControl\Entity\Resource')
                    ->exists($data['resource']);

                if ($data['resource'] == "0" || !$resource_exists) {
                    $msg = ['error' => 'Este recurso não existe.'];
                    return new ViewModel([
                        'form' => $form, 
                        'msg' => $msg,
                        'entity' => $entity
                    ]);
                }  

                if (!empty($data['name'])) {
                    $name_exists = $this->em->getRepository($this->entity)
                    ->nameEditExists($data['id'], $data['name'], $data['role'], $data['resource']);

                    if ($name_exists) {
                        $msg = ['error' => 'Este privilégio já existe.'];
                        return new ViewModel([
                            'form' => $form, 
                            'msg' => $msg,
                            'entity' => $entity
                        ]);
                    }
                } else {
                    $msg = ['error' => 'Informe um nome para o privilégio.'];
                    return new ViewModel([
                        'form' => $form, 
                        'msg' => $msg,
                        'entity' => $entity
                    ]);
                }  

                if (
                    !$this->securityEvent->securityVerify($data['role']) ||
                    !$this->securityEvent->securityVerify($data['resource']) ||
                    !$this->securityEvent->securityVerify($data['name']) 
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->update($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel(array('form' => $form));
    }

    public function deleteAction() {
        $conn = $this->em->getConnection();
        $query = "UPDATE privilege SET deleted=:deleted WHERE id=:id";
        $stmt  = $conn->prepare($query);
        $stmt->bindValue(":deleted", 1);
        $stmt->bindValue(":id", $this->params()->fromRoute('id', 0));

        if ($stmt->execute()) {

            $queryP = "DELETE FROM acl WHERE privilege_id=:privilege_id";
            $stmtP  = $conn->prepare($queryP);
            $stmtP->bindValue(":privilege_id", $this->params()->fromRoute('id', 0));
            $stmtP->execute();
 
            return $this->redirect()->toRoute($this->route, 
                ['controller' => $this->controller]);
        }
    }
}