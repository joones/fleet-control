<?php
namespace FleetControl\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use FleetControl\Form\Login as LoginForm;
use FleetControl\Event\PasswordEvent;

class AuthController extends AbstractActionController {
    
    public function indexAction() {
        $form = new LoginForm;
        $error = false;
        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $sessionStorage = new SessionStorage("FleetControl");
                $auth = new AuthenticationService();

                $auth->setStorage($sessionStorage);

                $authAdapter = $this->getServiceLocator()->get("FleetControl\Auth\Adapter");

                $authAdapter->setUsername($data['email']);
                $authAdapter->setPassword($data['password']);

                $result = $auth->authenticate($authAdapter);
       
                if ($result->isValid()) {
                    $sessionStorage->write($auth->getIdentity()['user']);

                    return $this->redirect()->toRoute("app-router/default");
                } else {
                    $error = true;
                }
            }
        }
        $model = new ViewModel();
        $model->setTerminal(true);
        $model->setVariables([
            'form' => $form,
            'error' => $error
        ]);
        return $model;
    } 
    
    public function logoutAction() {
        $auth = new AuthenticationService;
        $auth->setStorage(new SessionStorage("FleetControl"));
        $auth->clearIdentity();
        return $this->redirect()->toRoute('app-auth');
    }

    public function passwordRecoverAction()
    {
        $email = $this->params()->fromQuery('email');

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $repository = $em->getRepository('FleetControl\Entity\User');
        $entity = $repository->findByEmail($email);

        if ($entity) {

            $event = new PasswordEvent($em);
            $event->recover($email);

            $message = "Foi enviado um e-mail para <b>{$email}</b> com as instruções para recuperar a senha." ;
        } else {
            $message = "Email não cadastrado no sistema.";
           
        }   

        $model = new ViewModel();
        $model->setTerminal(true);
        $model->setVariables(['message' => $message]);
        return $model;
    }
}
