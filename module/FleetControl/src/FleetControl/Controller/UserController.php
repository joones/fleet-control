<?php

namespace FleetControl\Controller;

use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Core\Controller\CrudController;
use FleetControl\Event\PasswordEvent;
use FleetControl\Event\AclEvent;
use Doctrine\ORM\EntityManager;
use FleetControl\Event\SecurityEvent;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;

class UserController extends CrudController {
    
    protected $em;
    protected $authService;
    protected $securityEvent;

    public function __construct(EntityManager $em, $entity, $form, $service, $controller, $route) {
        $this->em         = $em;
        $this->entity     = $entity;
        $this->form       = $form;
        $this->service    = $service;
        $this->controller = $controller;
        $this->route      = $route;

        $this->securityEvent = new SecurityEvent($this->em);
    }

    public function getAuthService() {
        return $this->authService;
    }

    public function indexAction() {
        $sessionStorage = new SessionStorage("FleetControl");
        $this->authService = new AuthenticationService;
        $this->authService->setStorage($sessionStorage);

        if ($this->getAuthService()->hasIdentity()) {
            $identity = $this->getAuthService()->getIdentity()->getId();

            $userRepository = $this->em->getRepository($this->entity);
            $user = $userRepository->find($identity);
            
            $list = $userRepository->findUsers();
            $page = $this->params()->fromRoute('page');
        
            $paginator = new Paginator(new ArrayAdapter($list));
            $paginator->setCurrentPageNumber($page)
                    ->setDefaultItemCountPerPage(12);

            return new ViewModel(array('data' => $paginator, 'page' => $page));
        }
    }

    public function newAction() {
        $form = $this->form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                if (!empty($data['status'])) {
                    $data['status'] = '1';
                } else {
                    $data['status'] = '0';
                }  

                $email_exists = $this->em->getRepository($this->entity)
                    ->emailExists($data['email']);

                if ($email_exists) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => $this->controller,
                        'action' => 'new'
                    ]);
                }

                if (!empty($data['status'])) {
                    $data['status'] = '1';
                } else {
                    $data['status'] = '0';
                }  

                if (
                    !$this->securityEvent->securityVerify($data['name']) ||
                    !$this->securityEvent->securityVerify($data['email']) ||
                    !$this->securityEvent->securityVerify($data['password']) ||
                    !$this->securityEvent->securityVerify($data['confirmation']) ||
                    !$this->securityEvent->securityVerify($data['status'])
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->insert($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel(array('form' => $form));
    }
    
    public function editAction() {
        $msg = null;
        $request = $this->getRequest();
        $repository = $this->em->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));
        
        if ($this->params()->fromRoute('id', 0)) {
            $array = $entity->toArray();
            unset($array['password']);
            $this->form->setData($array);
        }
        
        if ($request->isPost()) {
            $this->form->setData($request->getPost());
            $this->form->getInputFilter()->remove('password');
            $this->form->getInputFilter()->remove('confirmation');
            $this->form->getInputFilter()->remove('role');

            if ($this->form->isValid()) {
                $data = $request->getPost()->toArray();

                $email_exists = $this->em->getRepository($this->entity)
                    ->emailEditExists($data['id'], $data['email']);

                if ($email_exists) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => $this->controller,
                        'action' => 'new'
                    ]);
                }

                if (!empty($data['status'])) {
                    $data['status'] = '1';
                } else {
                    $data['status'] = '0';
                }  

                if (
                    !$this->securityEvent->securityVerify($data['name']) ||
                    !$this->securityEvent->securityVerify($data['email']) ||
                    !$this->securityEvent->securityVerify($data['status'])
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->update($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel(array('form' => $this->form));
    }

    public function changePasswordAction()
    {
        $repository = $this->em->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));

        return new ViewModel(['user' => $entity]);
    }

    public function updatePasswordAction()
    {
        $params = $this->params()->fromQuery();

        $event = new PasswordEvent($this->em);
        $update = $event->update($params['id'], $params['pass']);
        
        return $update;
    }

    public function deleteAction() {
        $conn = $this->em->getConnection();
        $query = "UPDATE user SET deleted=:deleted WHERE id=:id";
        $stmt  = $conn->prepare($query);
        $stmt->bindValue(":deleted", 1);
        $stmt->bindValue(":id", $this->params()->fromRoute('id', 0));

        if ($stmt->execute()) {

            $queryP = "DELETE FROM acl WHERE user_id=:user_id";
            $stmtP  = $conn->prepare($queryP);
            $stmtP->bindValue(":user_id", $this->params()->fromRoute('id', 0));
            $stmtP->execute();

            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
        }
    }

    public function aclAction()
    {   
        $userId = $this->params()->fromRoute('id');
        $aclRepo = $this->em->getRepository('FleetControl\Entity\Acl');
        $privileges = $aclRepo->findByUser($userId);
        return new ViewModel(['acl' => $privileges]);
    }

    public function applyAclAction()
    {
        $params = $this->params()->fromQuery();

        $event = new AclEvent($this->em);
        $event->update($params['id'], $params['status']);

        $model = new ViewModel();
        $model->setTerminal(true);
        return $model;
    }
}