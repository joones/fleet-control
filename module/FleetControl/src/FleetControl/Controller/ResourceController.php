<?php 

namespace FleetControl\Controller;

use Core\Controller\CrudController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

class ResourceController extends CrudController
{
	public function __construct($entity, $form, $service, $controller, $route) {
        $this->entity     = $entity;
        $this->form       = $form;
        $this->service    = $service;
        $this->controller = $controller;
        $this->route      = $route;
    }
}