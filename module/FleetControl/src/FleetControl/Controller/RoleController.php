<?php 

namespace FleetControl\Controller;

use Core\Controller\CrudController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Doctrine\ORM\EntityManager;
use FleetControl\Event\SecurityEvent;

class RoleController extends CrudController
{
    protected $em; 
    protected $securityEvent;

	public function __construct(
        EntityManager $em, $entity, $form, $service, 
        $controller, $route
    ) {
        $this->em         = $em;
        $this->entity     = $entity;
        $this->form       = $form;
        $this->service    = $service;
        $this->controller = $controller;
        $this->route      = $route;

        $this->securityEvent = new SecurityEvent($this->em);
    }

    public function indexAction() {
        $list = $this->em
            ->getRepository($this->entity)
            ->findRoles();

        $page = $this->params()->fromRoute('page');

        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
            ->setDefaultItemCountPerPage(12);

        return new ViewModel(array('data' => $paginator, 'page' => $page));
    }

    public function newAction() {
        $msg = null;
        $form = $this->form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $parent_exists = $this->em->getRepository($this->entity)
                    ->roleExists($data['parent']);

                if ($data['parent'] != "0" && !$parent_exists) {
                    $msg = ['error' => 'Este perfil não existe.'];
                    return new ViewModel(['form' => $form, 'msg' => $msg]);
                }

                if (!empty($data['name'])) {
                    $name_exists = $this->em->getRepository($this->entity)
                    ->nameExists($data['name']);

                    if ($name_exists) {
                        $msg = ['error' => 'Este perfil já existe.'];
                        return new ViewModel(['form' => $form, 'msg' => $msg]);
                    }
                } else {
                    $msg = ['error' => 'Informe um nome para o perfil.'];
                    return new ViewModel(['form' => $form, 'msg' => $msg]);
                }   

                if (!empty($data['admin'])) {
                    $data['admin'] = '1';
                } else {
                    $data['admin'] = '0';
                }

                if (
                    !$this->securityEvent->securityVerify($data['name']) ||
                    !$this->securityEvent->securityVerify($data['parent']) ||
                    !$this->securityEvent->securityVerify($data['admin'])
                ){
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }
                
                $this->service->insert($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel(['form' => $form, 'msg' => $msg]);
    }

    public function editAction() {
        $msg = null;
        $form = $this->form;
        $request = $this->getRequest();
        
        $repository = $this->em->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));
        
        if ($entity->getId() == 1)
            return $this->redirect()->toRoute($this->route, [
                'controller' => 'role'
            ]);

        if ($this->params()->fromRoute('id', 0)) {
            $form->setData($entity->toArray());
        }
        
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $parent_exists = $this->em->getRepository($this->entity)
                    ->roleExists($data['parent']);

                if ($data['parent'] != "0" && !$parent_exists) {
                    $msg = ['error' => 'Este perfil não existe.'];
                    return new ViewModel([
                        'form' => $form, 
                        'msg' => $msg,
                        'entity' => $entity
                    ]);
                }

                if (!empty($data['name'])) {
                    $name_exists = $this->em->getRepository($this->entity)
                    ->nameEditExists($data['id'], $data['name']);

                    if ($name_exists) {
                        $msg = ['error' => 'Este perfil já existe.'];
                        return new ViewModel([
                            'form' => $form, 
                            'msg' => $msg,
                            'entity' => $entity
                        ]);
                    }
                } else {
                    $msg = ['error' => 'Informe um nome para o perfil.'];
                    return new ViewModel([
                        'form' => $form, 
                        'msg' => $msg,
                        'entity' => $entity
                    ]);
                } 

                if (!empty($data['admin'])) {
                    $data['admin'] = '1';
                } else {
                    $data['admin'] = '0';
                }  

                if (
                    !$this->securityEvent->securityVerify($data['name']) ||
                    !$this->securityEvent->securityVerify($data['parent']) ||
                    !$this->securityEvent->securityVerify($data['admin'])
                ){
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->update($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        
        return new ViewModel([
            'form' => $form, 
            'msg' => $msg,
            'entity' => $entity
        ]);
    }

    public function deleteAction() {
        $conn = $this->em->getConnection();
        $query = "UPDATE role SET deleted=:deleted WHERE id=:id AND id != :admin";
        $stmt  = $conn->prepare($query);
        $stmt->bindValue(":deleted", 1);
        $stmt->bindValue(":id", $this->params()->fromRoute('id', 0));
        $stmt->bindValue(":admin", 1);

        if ($stmt->execute()) {

            $privileges = $this->em->getRepository(
                'FleetControl\Entity\Privilege'
            )->findBy([
                'role' => $this->params()->fromRoute('id', 0),
                'deleted' => '0'
            ]);

            foreach ($privileges as $privilege) {
                $queryP = "UPDATE privilege SET deleted=:deleted WHERE role_id=:id";
                $stmtP  = $conn->prepare($queryP);
                $stmtP->bindValue(":deleted", 1);
                $stmtP->bindValue(":id", $this->params()->fromRoute('id', 0));
                $stmtP->execute();

                $queryU = "UPDATE user SET deleted=:deleted WHERE role_id=:role_id";
                $stmtU  = $conn->prepare($queryU);
                $stmtU->bindValue(":deleted", 1);
                $stmtU->bindValue(":role_id", $this->params()->fromRoute('id', 0));

                if ($stmtU->execute()) {
                    $queryACL = "DELETE FROM acl WHERE privilege_id=:privilege_id";
                    $stmtACL  = $conn->prepare($queryACL);
                    $stmtACL->bindValue(":privilege_id", $privilege->getId());
                    $stmtACL->execute();
                }
            }

            return $this->redirect()->toRoute($this->route, 
                ['controller' => $this->controller]);
        }
    }
}