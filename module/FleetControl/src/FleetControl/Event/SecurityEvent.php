<?php 

namespace FleetControl\Event;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;

class SecurityEvent implements EventManagerAwareInterface
{
	protected $events;
	private $conn;
	private $em;

    protected $authService;

    public function getAuthService() {
        return $this->authService;
    }

	public function __construct($em)
	{
		$this->em   = $em;
		$this->conn = $this->em->getConnection();
	}

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_class($this)
        ));
        $this->events = $events;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    public function getMaliciousLabels()
    {
        return [
            "''", '""', '"', "'", ";", "*", 'SELECT', 'SET', 'LIKE', 'ADD', 
            'UNION', 'INSERT', 'DELETE', '%', '--', 'execut', 'DROP', 'EXECUTE', 
            'JOIN'
        ];
    }

    public function splitOutput($output)
    {
        return preg_split('/ /', $output , -1, PREG_SPLIT_OFFSET_CAPTURE);
    }

    public function getUserIp()
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            return $_SERVER['HTTP_CLIENT_IP'];
        } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    public function getUserId()
    {
        $sessionStorage = new SessionStorage("FleetControl");
        $this->authService = new AuthenticationService;
        $this->authService->setStorage($sessionStorage);    
        
        if ($this->getAuthService()->hasIdentity()) {
            return $this->getAuthService()->getIdentity()->getId();
        } else {
            return false;
        }
    }

    public function securityVerify($output)
    {
        if (!empty($output)) {
            if (
                $output == strip_tags($output) && 
                filter_var($output, FILTER_SANITIZE_STRING) 
            ) {
                $split_names = $this->splitOutput($output);
                $malicious = $this->getMaliciousLabels();

                foreach ($split_names as $key => $split_name) {
                    if (!empty($split_name[0])) {
                        if (in_array($split_name[0], $malicious)) {
                            $attack = htmlentities($output);
                            $this->registerAttack($attack);
                            return false;
                        }
                    }
                }    

                return true;
            } else {
                $attack = htmlentities($output);
                $this->registerAttack($attack); 
                
                return false;
            }
        } else {
            return true;
        }
    }

    public function registerAttack($attack)
    {
        error_log("---------- TENTATIVA DE ATAQUE ----------");
        error_log("IP: " . $this->getUserIp()); 
        error_log("COD. USUARIO: " . $this->getUserId()); 
        error_log("TENTATIVA: " . $attack);
        error_log("------------------------------------------");

        try {
            $query = "INSERT INTO access_log (user_id, ip, attack, attack_time) 
                      VALUES (:user_id, :ip, :attack, :attack_time)";
             
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(":user_id", $this->getUserId());
            $stmt->bindValue(":ip", $this->getUserIp());
            $stmt->bindValue(":attack", $attack);
            $stmt->bindValue(":attack_time", date("Y-m-d H:i:s"));
             
            return $stmt->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}