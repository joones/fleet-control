<?php 

namespace FleetControl\Event;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;

class PasswordEvent implements EventManagerAwareInterface
{
	protected $events;
	private $conn;
	private $em;

	public function __construct($em)
	{
		$this->em   = $em;
		$this->conn = $this->em->getConnection();
	}

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_class($this)
        ));
        $this->events = $events;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    public function update($id, $password)
    {
    	$query = "UPDATE user SET password=:password WHERE id=:id";
    	$stmt  = $this->conn->prepare($query);
    	$stmt->bindValue(":password", $password);
    	$stmt->bindValue(":id", $id);
    	return $stmt->execute();
    }

    public function recover($email)
    {
        $passwordEmail = time();
        $passwordDb    = md5($passwordEmail);

        $query = "UPDATE user SET password=:password WHERE email=:email";
        $stmt  = $this->conn->prepare($query);
        $stmt->bindValue(":password", $passwordDb);
        $stmt->bindValue(":email", $email);

        if ($stmt->execute()) {
            
            $message = new Message();
            $message->addTo($email)
                ->addFrom('naoresponda.leadscontrol@gmail.com')
                ->setSubject('Recuperação de senha - Leads Control');
                
            // Setup SMTP transport using LOGIN authentication
            $transport = new SmtpTransport();
            $options   = new SmtpOptions(array(
                'host'              => 'smtp.gmail.com',
                'connection_class'  => 'login',
                'connection_config' => array(
                    'ssl'       => 'tls',
                    'username' => 'naoresponda.leadscontrol@gmail.com',
                    'password' => 'NrLc#2015'
                ),
                'port' => 587,
            ));

            $html = new MimePart('Sua senha temporária de acesso ao sistema é <b>' . $passwordEmail . '</b>');
            $html->type = "text/html";

            $body = new MimeMessage();
            $body->addPart($html);

            $message->setBody($body);

            $transport->setOptions($options);
            $transport->send($message);
        }
    }
}