<?php 

namespace FleetControl\Event;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;

class AclEvent implements EventManagerAwareInterface
{
	protected $events;
	private $conn;
	private $em;

	public function __construct($em)
	{
		$this->em   = $em;
		$this->conn = $this->em->getConnection();
	}

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_class($this)
        ));
        $this->events = $events;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    public function register($id, $role)
    {
        $privileges = $this->em->getRepository('FleetControl\Entity\Privilege')
            ->findBy([
                'role' => $role,
                'deleted' => '0'
            ]);

        foreach ($privileges as $privilege) {
        	$query = "INSERT INTO acl (privilege_id, user_id, status)
                            VALUES (:privilege_id, :user_id, :status)";

        	$stmt  = $this->conn->prepare($query);
            $stmt->bindValue(":privilege_id", $privilege->getId());
        	$stmt->bindValue(":user_id", $id);
        	$stmt->bindValue(":status", '0');
        	$stmt->execute();
        }
    }

    public function privilegeService($privilege, $role)
    {
        $users = $this->em->getRepository('FleetControl\Entity\User')->findByRole($role);

        foreach ($users as $user) {
            $query = "INSERT INTO acl (privilege_id, user_id, status)
                            VALUES (:privilege_id, :user_id, :status)";

            $stmt  = $this->conn->prepare($query);
            $stmt->bindValue(":privilege_id", $privilege);
            $stmt->bindValue(":user_id", $user->getId());
            $stmt->bindValue(":status", '0');
            $stmt->execute();
        }
    }

    public function update($id, $status) 
    {
        $query = "UPDATE acl SET status=:status WHERE id=:id";
        $stmt  = $this->conn->prepare($query);
        $stmt->bindValue(":status", $status);
        $stmt->bindValue(":id", $id);
        return $stmt->execute();
    }
}