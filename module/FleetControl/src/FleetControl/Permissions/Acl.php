<?php 

namespace FleetControl\Permissions;

use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole as ZendRole;
use Zend\Permissions\Acl\Resource\GenericResource as ZendResource;

class Acl extends ZendAcl
{
	private $em;
	protected $roles;
	protected $resources;
	protected $privileges;

	public function __construct(array $roles, array $resources, array $privileges)
	{
		$this->roles = $roles;
		$this->resources = $resources;
		$this->privileges = $privileges;

		$this->laodRoles();
		$this->loadResources();
		$this->loadPrivileges();
	}

	protected function laodRoles()
	{
		foreach ($this->roles as $role) {

			if ($role->getParent()) {
				$this->addRole(
					new ZendRole($role->getName()),
					new ZendRole($role->getParent()->getName())
				);
			} else {
				$this->addRole(new ZendRole($role->getName()));
			}
			
			if ($role->getAdmin()) {
				$this->allow($role->getName(), array(), array());
			}
		}
	}

	protected function loadResources()
	{
		foreach ($this->resources as $resource) {
			$this->addResource(new ZendResource($resource->getName()));
		}
	}

	protected function loadPrivileges()
	{
		foreach ($this->privileges as $privilege) {
			$this->allow(
				$privilege->getRole()->getName(),
				$privilege->getResource()->getName(),
				$privilege->getName()
			);
		}
	}
}