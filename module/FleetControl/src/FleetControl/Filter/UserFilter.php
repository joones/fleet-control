<?php  

namespace FleetControl\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;

class UserFilter extends InputFilter
{
	public function __construct()
	{
		$this->add([
			'name' => 'name',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo NOME.',
						]
					]
				]
			]
		]);


		$this->add([
			'name' => 'email',
			'validators' => [
				[
			        'name' => 'EmailAddress',
			        'options' => [
                        'required'  => true,
			            'messages' => [
			                'emailAddressInvalidFormat' => "Digite um EMAIL VÁLIDO.",

			            ]
			        ],
			    ],
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo EMAIL.',
						]
					]
				],
				
			]
		]);

		$this->add([
			'name' => 'password',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo SENHA.'
						]
					]
				]
			]
		]);

		$this->add([
			'name' => 'confirmation',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
			
				[
					'name' => 'Identical',
					'options' => [
						'token' => 'password',
						'message' => 'Senhas não são identicas.'
						
					]
				],
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo CONFIRMAR SENHA.'
						]
					]
				]
			]
		]);
	}
}