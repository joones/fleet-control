<?php  

namespace FleetControl\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;

class RoleFilter extends InputFilter
{
	public function __construct()
	{
		$this->add([
			'name' => 'name',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Informe um nome para o perfil.',
						]
					]
				]
			]
		]);
	}
}