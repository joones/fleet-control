<?php

namespace FleetControl\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;
use FleetControl\Filter\RoleFilter;

class Role extends Form 
{
	protected $parent;

    public function __construct($name = null, $parent = [], $options = []) 
    {
        parent::__construct($name, $options);

        $this->setInputFilter(new RoleFilter());
        $this->parent = $parent;

        $this->setAttributes(array('method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-label-left'));
        
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $name = new \Zend\Form\Element\Text('name');
        $name->setLabel("Nome: ")
                ->setLabelAttributes(array('class' => 'col-md-6', 'style' => 'margin-top: 1%'))
                ->setAttributes(array('placeholder' => 'Digite o nome', 'class' => 'form-control'));
    	$this->add($name);
        
    	$allParent = array_merge([0 => 'Nenhum'], $this->parent);
    	$parent = new Select();
    	$parent->setLabel('Herança: ')
                ->setLabelAttributes(array('class' => 'col-md-6', 'style' => 'margin-top: 1%'))
    			->setName("parent")
                ->setAttributes(['class' => 'form-control'])
    			->setOptions([
    				'value_options' => $allParent
    			]);
    	$this->add($parent);

    	$csfr = new \Zend\Form\Element\Csrf('security');
        $this->add($csfr);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabel(' ')
                ->setAttributes(array(
                    'value' => 'SALVAR PERFIL',
                    'class' => 'btn btn-success btn-round'
                ));
        $this->add($submit);

        $button = new \Zend\Form\Element\Button('button');
        $button->setLabel('CANCELAR')
                ->setAttributes(array(
                    'class' => 'btn btn-default btn-round',
                    'id' => 'btn-cancel'
                ));
        $this->add($button);
    }
}