<?php
namespace FleetControl\Form;

use Zend\Form\Form;

class Login extends Form {
    
    public function __construct($name = null, $options = array()) {
        parent::__construct('Login', $options);
        
        $this->setAttribute('method', 'post');
        
        $email = new \Zend\Form\Element\Text("email");
        $email->setLabel("Email: ")
                ->setAttributes(array(
                    'class' => 'form-control', 
                    'placeholder' => 'Seu email', 
                    'required',
                    'style' => 'text-align: center;'
                ));
        $this->add($email);
        
        $password = new \Zend\Form\Element\Password("password");
        $password->setLabel("Senha: ")
                ->setAttributes(array(
                    'class' => 'form-control', 
                    'placeholder' => 'Sua senha', 
                    'required',
                    'style' => 'text-align: center;'
                ));
        $this->add($password);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabel(' ')
                ->setAttributes(array(
                    'value' => 'ACESSAR SISTEMA',
                    'class' => 'btn btn-success submit',
                    'style' => 'margin-left: 30%'
                ));
        $this->add($submit);
        
    }
}
