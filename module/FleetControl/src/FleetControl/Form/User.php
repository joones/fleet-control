<?php

namespace FleetControl\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;
use FleetControl\Filter\UserFilter;

class User extends Form {
    
    public function __construct($name = null, $profiles = [], $statusList = [], $options = []) {
        parent::__construct('user', $options);

        $this->setInputFilter(new UserFilter());
        $this->setAttributes(array('method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-label-left'));
        
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $profile = new Select();
        $profile->setLabel("Perfil: ")
                ->setName('role')
                ->setAttribute('class', 'form-control')
                ->setValueOptions($profiles);
        $this->add($profile);
        
        $name = new \Zend\Form\Element\Text('name');
        $name->setLabel("Nome: ")
                ->setAttributes(array('placeholder' => 'Digite o nome', 'class' => 'form-control'));
                
        $this->add($name);
        $email = new \Zend\Form\Element\Email('email');
        $email->setLabel("Email: ")
                ->setAttributes(array('placeholder' => 'Digite o email', 'class' => 'form-control'));
        $this->add($email);
        
        $password = new \Zend\Form\Element\Password('password');
        $password->setLabel('Senha: ')
                ->setAttributes(array('placeholder' => 'Digite a senha', 'class' => 'form-control'));
        $this->add($password);
        
        $confirmPassword = new \Zend\Form\Element\Password('confirmation');
        $confirmPassword->setLabel('Confimar senha: ')
                ->setAttributes(array('placeholder' => 'Redigite a senha', 'class' => 'form-control'));
        $this->add($confirmPassword);
    
        $csfr = new \Zend\Form\Element\Csrf('security');
        $this->add($csfr);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabel(' ')
                ->setAttributes(array(
                    'value' => 'Salvar',
                    'class' => 'btn btn-success',
                ));
        $this->add($submit);
    }
}