<?php

namespace FleetControl\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;

class Resource extends Form 
{
    public function __construct($name = null, $options = []) 
    {
        parent::__construct($name, $options);

        $this->setAttributes(array('method' => 'post', 'role' => 'form'));
        
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $name = new \Zend\Form\Element\Text('name');
        $name->setLabel("Nome: ")
                ->setLabelAttributes(array('class' => 'col-md-6', 'style' => 'margin-top: 1%'))
                ->setAttributes(array('placeholder' => 'Digite o nome', 'class' => 'form-control'));
    	$this->add($name);
        
    	$csfr = new \Zend\Form\Element\Csrf('security');
        $this->add($csfr);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabel(' ')->setLabelAttributes(array('style' => 'margin-left: 2.8%; margin-right: 1%;'))
                ->setAttributes(array(
                    'value' => 'Salvar',
                    'class' => 'btn',
                    'style' => 'background: #333; color: white;'
                ));
        $this->add($submit);

        $button = new \Zend\Form\Element\Button('button');
        $button->setLabel('Voltar')
                ->setAttributes(array(
                    'value' => 'Voltar',
                    'class' => 'btn btn-default',
                    'id' => 'btn_cancel_request'
                ));
        $this->add($button);
    }
}