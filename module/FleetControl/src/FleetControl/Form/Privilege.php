<?php

namespace FleetControl\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;
use FleetControl\Filter\PrivilegeFilter;

class Privilege extends Form 
{
    public function __construct($name = null, $roles = [], $resources = [], $options = []) 
    {
        parent::__construct($name, $options);
        $this->setInputFilter(new PrivilegeFilter());

        $this->setAttributes(array('method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-label-left'));
        
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $name = new \Zend\Form\Element\Text('name');
        $name->setLabel("Privilégio: ")
                ->setLabelAttributes(array('class' => 'col-md-6', 'style' => 'margin-top: 1%'))
                ->setAttributes(array('placeholder' => 'Digite o privilégio', 'class' => 'form-control'));
    	$this->add($name);
        
        $role = new Select();
        $role->setLabel('Perfil: ')
                ->setLabelAttributes(array('class' => 'col-md-6', 'style' => 'margin-top: 1%'))
                ->setName("role")
                ->setAttributes(['class' => 'form-control'])
                ->setOptions([
                    'value_options' => $roles
                ]);
        $this->add($role);

        $resource = new Select();
        $resource->setLabel('Recurso: ')
                ->setLabelAttributes(array('class' => 'col-md-6', 'style' => 'margin-top: 1%'))
                ->setName("resource")
                ->setAttributes(['class' => 'form-control'])
                ->setOptions([
                    'value_options' => $resources
                ]);
        $this->add($resource);

    	$csfr = new \Zend\Form\Element\Csrf('security');
        $this->add($csfr);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabel(' ')
                ->setAttributes(array(
                    'value' => 'SALVAR PRIVILÉGIO',
                    'class' => 'btn btn-success btn-round'
                ));
        $this->add($submit);

        $button = new \Zend\Form\Element\Button('button');
        $button->setLabel('CANCELAR')
                ->setAttributes(array(
                    'class' => 'btn btn-default btn-round',
                    'id' => 'btn-cancel'
                ));
        $this->add($button);
    }
}