<?php 

namespace FleetControl\Repository;

use Doctrine\ORM\EntityRepository;

class ResourceRepository extends EntityRepository
{
	public function fetchResources()
	{
		$entities = $this->findAll();
		$array = [];

		foreach ($entities as $entity) {
			$array[$entity->getId()] = $entity->getName();
		}

		return $array;
	}

	public function exists($id) 
    {
        $entity = $this->find($id);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }
}