<?php 

namespace FleetControl\Repository;

use Doctrine\ORM\EntityRepository;

class PrivilegeRepository extends EntityRepository
{
	public function findPrivileges()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0')
                $array[] = $entity;
        }

        return $array;
    }

    public function nameExists($name, $role, $resource) 
    {
        $entity = $this->findOneBy([
            'role' => $role,
            'resource' => $resource,
            'name' => $name, 
            'deleted' => '0'
        ]);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

    public function nameEditExists($id, $name, $role, $resource) 
    {
        $entity = $this->findOneBy([
            'role' => $role,
            'resource' => $resource,
            'name' => $name, 
            'deleted' => '0'
        ]);
        if ($entity) {
            if ($entity->getId() != $id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}