<?php 

namespace FleetControl\Repository;

use Doctrine\ORM\EntityRepository;

class AclRepository extends EntityRepository
{
	public function fetchResources()
	{
		$entities = $this->findAll();
		$array = [];

		foreach ($entities as $entity) {
			$array[$entity->getId()] = $entity->getName();
		}

		return $array;
	}
}