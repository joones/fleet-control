<?php 

namespace FleetControl\Repository;

use Doctrine\ORM\EntityRepository;

class RoleRepository extends EntityRepository
{
	public function fetchParent()
	{
		$entities = $this->findAll();
		$array = [];

		foreach ($entities as $entity) {
            if ($entity->getDeleted() != 1)
			    $array[$entity->getId()] = $entity->getName();
		}

		return $array;
	}

	public function fetchProfiles()
	{
		$entities = $this->findAll();
		$array = [];

		foreach ($entities as $entity) {
			if ($entity->getAdmin() != 1) {
                if ($entity->getDeleted() == '0')
				    $array[$entity->getId()] = $entity->getName();
            }  
		}

		return $array;
	}

    public function fetchFormProfiles()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0')
                $array[$entity->getId()] = $entity->getName();
        }

        return $array;
    }


    public function roleExists($parent) 
    {
        $entity = $this->findOneBy(['id' => $parent, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

	public function parentExists($parent) 
    {
        $entity = $this->findOneBy(['parent' => $parent, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

    public function nameExists($name) 
    {
        $entity = $this->findOneBy(['name' => $name, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

    public function nameEditExists($id, $name) 
    {
        $entity = $this->findOneBy(['name' => $name, 'deleted' => '0']);
        if ($entity) {
        	if ($entity->getId() != $id) {
            	return true;
        	} else {
        		return false;
        	}
        } else {
            return false;
        }
    }

    public function findRoles()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0')
                $array[] = $entity;
        }

        return $array;
    }
}