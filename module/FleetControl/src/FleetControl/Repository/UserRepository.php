<?php

namespace FleetControl\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{	
	public function findByEmailAndPassword($email, $password) {
        $user = $this->findOneByEmail($email);
    	if ($user) {
    		$hashSenha = md5($password);

            if ($user->getStatus() == 1) {
                if ($hashSenha == $user->getPassword()) {
                    return $user;
                } else {
                    return false;
                }
            } else {
                return false;
            }
    	} else {
    		return false;
    	}
    }

    public function findArray() {
    	$users = $this->findAll();
    	$a = array();

    	foreach ($users as $user) {
    		$a[$user->getId()]['id'] = $user->getId();
    		$a[$user->getNome()]['nome'] = $user->getNome();
    		$a[$user->getEmail()]['email'] = $user->getEmail();
    	}

    	return $a;
    }

	public function findUsers()
	{
		$entities = $this->findAll();
		$array = [];

		foreach ($entities as $entity) {
			if ($entity->getDeleted() == '0')
				$array[] = $entity;
		}

		return $array;
	}

    public function fetchUsers()
    {
        $entities = $this->findAll();
        $array = [];

        $array[0] = "Selecinar";
        
        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0')
                $array[$entity->getId()] = $entity->getName();
        }

        return $array;
    }

    public function emailExists($email) 
    {
        $entity = $this->findOneBy(['email' => $email, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

    public function emailEditExists($id, $email) 
    {
        $entity = $this->findOneBy(['email' => $email, 'deleted' => '0']);
        if ($entity) {
            if ($entity->getId() != $id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function userExists($id) 
    {
        $entity = $this->findOneBy(['id' => $id, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }
}