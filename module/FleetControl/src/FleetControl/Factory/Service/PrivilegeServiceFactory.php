<?php

namespace FleetControl\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Service\Privilege;

class PrivilegeServiceFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {	
   		$em = $controllerManager->get('Doctrine\ORM\EntityManager');

        $service = new Privilege($em, 'FleetControl\Entity\Privilege');
    	
        return $service;
    }
}