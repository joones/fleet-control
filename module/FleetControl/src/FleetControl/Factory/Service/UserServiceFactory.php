<?php

namespace FleetControl\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Service\User;

class UserServiceFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {	
   		$em = $controllerManager->get('Doctrine\ORM\EntityManager');

        $service = new User($em, 'FleetControl\Entity\User');
    	
        return $service;
    }
}