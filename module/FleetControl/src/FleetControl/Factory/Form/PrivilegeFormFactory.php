<?php

namespace FleetControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Form\Privilege;

class PrivilegeFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$em = $controllerManager->get('Doctrine\ORM\EntityManager');

   		$roleRepository = $em->getRepository('FleetControl\Entity\Role');
   		$roles = $roleRepository->fetchProfiles();

   		$resourceRepository = $em->getRepository('FleetControl\Entity\Resource');
   		$resources = $resourceRepository->fetchResources();

        $form = new Privilege('privilege', $roles, $resources);
        return $form;
    }
}