<?php

namespace FleetControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Form\Role;

class RoleFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$em = $controllerManager->get('Doctrine\ORM\EntityManager');

   		$roleRepository = $em->getRepository('FleetControl\Entity\Role');
   		$parent = $roleRepository->fetchParent();

        $form = new Role('role', $parent);
        return $form;
    }
}