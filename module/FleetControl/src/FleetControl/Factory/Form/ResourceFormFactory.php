<?php

namespace FleetControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Form\Resource;

class ResourceFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
        $form = new Resource('resource');
        return $form;
    }
}