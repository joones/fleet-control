<?php

namespace FleetControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Form\User;

class UserFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$em = $controllerManager->get('Doctrine\ORM\EntityManager');
    	
    	$roleRepository = $em->getRepository('FleetControl\Entity\Role');
   		$roles = $roleRepository->fetchFormProfiles();

        $status = [
            1 => 'Ativo',
            0 => 'Inativo'
        ];

        $form = new User('user', $roles, $status);
        return $form;
    }
}