<?php

namespace FleetControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Controller\UserController;

class UserControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');
    	$form = $serviceManager->get('FleetControl\Form\User');
    	$service = $serviceManager->get('FleetControl\Service\User');

        $controller = new UserController(
    		$em, 'FleetControl\Entity\User', $form, $service, 'user', 'app-router/default'   
    	);
    	
        return $controller;
    }
}