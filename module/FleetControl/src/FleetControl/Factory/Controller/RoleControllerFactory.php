<?php

namespace FleetControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Controller\RoleController;

class RoleControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');
    	$form = $serviceManager->get('FleetControl\Form\Role');
    	$service = $serviceManager->get('FleetControl\Service\Role');

        $controller = new RoleController(
    		$em, 'FleetControl\Entity\Role', $form, $service, 'role', 'app-router/default'
    	);
    	
        return $controller;
    }
}