<?php

namespace FleetControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Controller\PrivilegeController;

class PrivilegeControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');
    	$form = $serviceManager->get('FleetControl\Form\Privilege');
    	$service = $serviceManager->get('FleetControl\Service\Privilege');

        $controller = new PrivilegeController(
    		$em, 'FleetControl\Entity\Privilege', $form, $service, 'privilege', 'app-router/default'
    	);
    	
        return $controller;
    }
}