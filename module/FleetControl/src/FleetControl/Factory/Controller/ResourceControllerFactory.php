<?php

namespace FleetControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Controller\ResourceController;

class ResourceControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

    	$form = $serviceManager->get('FleetControl\Form\Resource');
    	$service = $serviceManager->get('FleetControl\Service\Resource');

        $controller = new ResourceController(
    		'FleetControl\Entity\Resource', $form, $service, 'resource', 'app-router/default'
    	);
    	
        return $controller;
    }
}