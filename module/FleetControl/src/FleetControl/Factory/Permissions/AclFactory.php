<?php

namespace FleetControl\Factory\Permissions;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use FleetControl\Permissions\Acl;

class AclFactory implements FactoryInterface	
{
	  public function createService(ServiceLocatorInterface $controllerManager)
    {
    	  $em = $controllerManager->get('Doctrine\ORM\EntityManager');

   		  $roleRepository = $em->getRepository('FleetControl\Entity\Role');
   		  $roles = $roleRepository->findAll();

   		  $resourceRepository = $em->getRepository('FleetControl\Entity\Resource');
   		  $resources = $resourceRepository->findAll();

   		  $privilegeRepository = $em->getRepository('FleetControl\Entity\Privilege');
   		  $privileges = $privilegeRepository->findAll();
    
        $acl = new Acl($roles, $resources, $privileges);
        return $acl;
    }
}