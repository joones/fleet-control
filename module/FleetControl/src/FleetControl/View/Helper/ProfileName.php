<?php

namespace FleetControl\View\Helper;

use Zend\View\Helper\AbstractHelper;

class ProfileName extends AbstractHelper {

    public function __invoke($profile) {
        switch ($profile) {
            case 'R':
                $profielName = "Root";
                break;
            case 'A':
                $profielName = "Administrador";
                break;
            case 'S':
                $profielName = "Supervisor";
                break;
            case 'C':
                $profielName = "Corretor";
                break;
            default:
                break;
        }

        return $profielName;
    }

}
