<?php

namespace FleetControl\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;

class CompleteUserIdentity extends AbstractHelper {

    protected $authService;

    public function getAuthService() {
        return $this->authService;
    }

    public function __invoke($namespace = null) {
        $sessionStorage = new SessionStorage($namespace);
        $this->authService = new AuthenticationService;
        $this->authService->setStorage($sessionStorage);

        if ($this->getAuthService()->hasIdentity()) {
            $identity = $this->getAuthService()->getIdentity()->getId();

            $sm = $this->getView()->getHelperPluginManager()->getServiceLocator();
            $em = $sm->get('Doctrine\ORM\EntityManager');

            $userRepository = $em->getRepository('FleetControl\Entity\User');
            $user = $userRepository->find($identity);

            return $user;
        }
        else {
            return false;
        }
    }

}
