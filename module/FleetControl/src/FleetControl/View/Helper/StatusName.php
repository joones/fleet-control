<?php

namespace FleetControl\View\Helper;

use Zend\View\Helper\AbstractHelper;

class StatusName extends AbstractHelper {

    public function __invoke($status) {
        switch ($status) {
            case 1:
                $statusName = "Ativo";
                break;
            case 0:
                $statusName = "Inativo";
                break;
            default:
                break;
        }

        return $statusName;
    }

}
