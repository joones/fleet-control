<?php

namespace FleetControl\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;

class AclPermission extends AbstractHelper {

    public function __invoke($role, $privilege, $resource) {
    
        $sm = $this->getView()->getHelperPluginManager()->getServiceLocator();
        $acl = $sm->get('FleetControl\Permissions\Acl');

        $allowed =  $acl->isAllowed($role, $resource, $privilege);

        if ($allowed) {
            $em = $sm->get('Doctrine\ORM\EntityManager');
            
            $roleRepo = $em->getRepository('FleetControl\Entity\Role')->findOneByName($role);
            
            if ($roleRepo->getAdmin() != 1) {
                $resourceRepo    = $em->getRepository('FleetControl\Entity\Resource')->findOneByName($resource);
                $privilegeRepo   = $em->getRepository('FleetControl\Entity\Privilege')->findOneBy([
                    'role' => $roleRepo->getId(),
                    'resource' => $resourceRepo->getId(),
                    'name' => $privilege
                ]);
                $sessionStorage = new SessionStorage("FleetControl");
                $auth = new AuthenticationService;
                $auth->setStorage($sessionStorage);

                if ($auth->hasIdentity()) {
                    $user = $auth->getIdentity();

                    $aclRepo = $em->getRepository('FleetControl\Entity\Acl')->findOneBy([
                        'privilege' => $privilegeRepo->getId(),
                        'user' => $user->getId()
                    ]);

                    if ($aclRepo) {
                        if ($aclRepo->getStatus() == '1') {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
