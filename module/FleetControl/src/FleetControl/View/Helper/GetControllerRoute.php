<?php

namespace FleetControl\View\Helper;

use Zend\View\Helper\AbstractHelper;

class GetControllerRoute extends AbstractHelper {

    public function __invoke() {
        $serviceLocator = $this->getView()->getHelperPluginManager()->getServiceLocator();
        $routeMatch = $serviceLocator->get('Application')->getMvcEvent()->getRouteMatch();

        if (!empty($routeMatch)) {
            $params = $routeMatch->getParams();

            if (isset($params['__CONTROLLER__'])) {
                return $params['__CONTROLLER__'];
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

}
