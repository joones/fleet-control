<?php echo $this->doctype(); ?>
<html>
<head>
    <title>Fleet Control</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="<?php echo $this->basePath(); ?>/css/bootstrap-select.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo $this->basePath(); ?>/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo $this->basePath(); ?>/css/style.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo $this->basePath(); ?>/css/font-awesome.css" rel="stylesheet"> 
    <script src="<?php echo $this->basePath(); ?>/js/jquery.min.js"> </script>
    
    <script src="<?php echo $this->basePath(); ?>/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $this->basePath(); ?>/js/jquery.metisMenu.js"></script>
    <script src="<?php echo $this->basePath(); ?>/js/jquery.slimscroll.min.js"></script>
    <link href="<?php echo $this->basePath(); ?>/css/custom.css" rel="stylesheet">
    <script src="<?php echo $this->basePath(); ?>/js/custom.js"></script>
    <script src="<?php echo $this->basePath(); ?>/js/screenfull.js"></script>
    <link href="<?php echo $this->basePath(); ?>/css/bootstrap-switch.css" rel="stylesheet">
    <script src="<?php echo $this->basePath(); ?>/js/bootstrap-switch.js"></script>
    <script src="<?php echo $this->basePath(); ?>/js/jquery.mask.js"></script>
    <script src="<?php echo $this->basePath(); ?>/js/jquery.maskMoney.js"></script>
    <link href="<?php echo $this->basePath(); ?>/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="<?php echo $this->basePath(); ?>/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function(){
        $('#table').DataTable({
            "bPaginate": false,
        });
    });
    </script>
    <script src="<?php echo $this->basePath(); ?>/js/skycons.js"></script>
</head>
<body>
    <?php if ($this->GetControllerRoute() != "cashier-app" && $this->GetControllerRoute() != "order-item-app"): ?>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" 
                    data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1> 
                    <a class="navbar-brand" 
                        href="<?php echo $this->url('app-router/default', 
                        ['controller' => 'index']); ?>">
                        Fleet Control
                    </a>
                </h1>         
            </div>
            <div class=" border-bottom">
             
                <div class="drop-men" >
                    <ul class=" nav_1">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown">
                                <span class=" name-caret">
                                    <?php echo $this->UserIdentity("FleetControl")->getName(); ?> 
                                    <i class="caret"></i>
                                </span>
                                <img src="https://image.freepik.com/icones-gratis/imagem-do-usuario-com-fundo-preto_318-34564.png" width="68">
                            </a>
                            <ul class="dropdown-menu " role="menu">
                                <li>
                                    <a href="<?php echo $this->url('app-router/default', 
                                        ['controller' => 'auth', 'action' => 'logout']); ?>">
                                        <i class="fa fa-remove"></i>
                                        Sair
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="clearfix">
            </div>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo $this->url('app-router/default', 
                                ['controller' => 'index']); ?>" 
                                class=" hvr-bounce-to-right">
                                <i class="fa fa-dashboard nav_icon"></i>
                                <span class="nav-label">Dashboard</span> 
                            </a>
                        </li>
                        <?php if ($this->CompleteUserIdentity("FleetControl")->getRole()->getAdmin() == 1): ?>
                            <li>
                                <a href="#" class=" hvr-bounce-to-right">
                                    <i class="fa fa-check-square nav_icon"></i>
                                    <span class="nav-label">Controle de Acesso</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse" aria-expanded="true">
                                    <li>
                                        <a href="<?php echo $this->url('app-router/default', ['controller' => 'role']); ?>" 
                                            class="hvr-bounce-to-right">
                                            <i class="fa fa-puzzle-piece nav_icon"></i>Perfis
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $this->url('app-router/default', ['controller' => 'privilege']); ?>" 
                                            class="hvr-bounce-to-right">
                                            <i class="fa fa-unlock nav_icon"></i>Privilégios
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?> 
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Frente de Caixa', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', 
                                    ['controller' => 'cashier-app']); ?>" target="_blank" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-shopping-cart nav_icon"></i>
                                    <span class="nav-label">Frente de Caixa</span> 
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Pedidos', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', 
                                    ['controller' => 'order-item-app']); ?>" target="_blank" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-shopping-basket nav_icon"></i>
                                    <span class="nav-label">Pedidos</span> 
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Vendas', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', ['controller' => 'sale']); ?>" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-suitcase nav_icon"></i>
                                    <span class="nav-label">Vendas</span> 
                                </a>
                            </li>
                        <?php endif; ?> 
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Caixa', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', ['controller' => 'cashier']); ?>" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-archive nav_icon"></i>
                                    <span class="nav-label">Caixa</span> 
                                </a>
                            </li>
                        <?php endif; ?> 
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Contas a Pagar', 'Módulo'
                        ) || $this->AclPermission(
                             $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                             'Contas a Receber', 'Módulo'
                        )): ?>
                            <li>
                                <a href="#" class=" hvr-bounce-to-right">
                                    <i class="fa fa-cube nav_icon"></i>
                                    <span class="nav-label">Contas</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse" aria-expanded="true">
                                    <?php 
                                    if ($this->AclPermission(
                                        $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                                        'Contas a Pagar', 'Módulo'
                                    )): ?>
                                    <li>
                                        <a href="<?php echo $this->url('app-router/default', 
                                            ['controller' => 'account', 'action' => 'payable']); ?>" 
                                            class="hvr-bounce-to-right">
                                            <i class="fa fa-th-list nav_icon"></i>A Pagar
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    <?php 
                                    if ($this->AclPermission(
                                        $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                                        'Contas a Receber', 'Módulo'
                                    )): ?>
                                    <li>
                                        <a href="<?php echo $this->url('app-router/default', 
                                            ['controller' => 'account', 'action' => 'receivable']); ?>" 
                                            class="hvr-bounce-to-right">
                                            <i class="fa fa-tags nav_icon"></i>A Receber
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php endif; ?> 
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Clientes', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', ['controller' => 'customer']); ?>" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-users nav_icon"></i>
                                    <span class="nav-label">Clientes</span> 
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Funcionários', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', ['controller' => 'employee']); ?>" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-child nav_icon"></i>
                                    <span class="nav-label">Funcionários</span> 
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Estoque', 'Módulo'
                        )): ?>
                            <li>
                                <a href="#" class=" hvr-bounce-to-right">
                                    <i class="fa fa-cube nav_icon"></i>
                                    <span class="nav-label">Estoque</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse" aria-expanded="true">
                                    <li>
                                        <a href="<?php echo $this->url('app-router/default', ['controller' => 'inventory']); ?>" 
                                            class="hvr-bounce-to-right">
                                            <i class="fa fa-th-list nav_icon"></i>Itens
                                        </a>
                                    </li>
                                    <?php 
                                    if ($this->AclPermission(
                                        $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                                        'Categorias', 'Módulo'
                                    )): ?>
                                    <li>
                                        <a href="<?php echo $this->url('app-router/default', ['controller' => 'category']); ?>" 
                                            class="hvr-bounce-to-right">
                                            <i class="fa fa-tags nav_icon"></i>Categorias
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    <?php 
                                    if ($this->AclPermission(
                                        $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                                        'Subcategorias', 'Módulo'
                                    )): ?>
                                    <li>
                                        <a href="<?php echo $this->url('app-router/default', ['controller' => 'subcategory']); ?>" 
                                            class="hvr-bounce-to-right">
                                            <i class="fa fa-tags nav_icon"></i>Subcategorias
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php endif; ?>  
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Serviços', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', ['controller' => 'service']); ?>" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-book nav_icon"></i>
                                    <span class="nav-label">Serviços</span> 
                                </a>
                            </li>
                        <?php endif; ?> 
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Ordem de Serviços', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', ['controller' => 'order-service']); ?>" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-book nav_icon"></i>
                                    <span class="nav-label">Ordem de Serviços</span> 
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php 
                        if ($this->AclPermission(
                            $this->CompleteUserIdentity("FleetControl")->getRole()->getName(), 
                            'Relatórios', 'Módulo'
                        )): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', ['controller' => 'report']); ?>" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-file nav_icon"></i>
                                    <span class="nav-label">Relatórios</span> 
                                </a>
                            </li>
                        <?php endif; ?>  
                        <?php if ($this->CompleteUserIdentity("FleetControl")->getRole()->getAdmin() == 1): ?>
                            <li>
                                <a href="<?php echo $this->url('app-router/default', ['controller' => 'user']); ?>" 
                                    class=" hvr-bounce-to-right">
                                    <i class="fa fa-users nav_icon"></i>
                                    <span class="nav-label">Usuários</span> 
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="content-main">
                <?php endif; ?>
                <?php echo $this->content; ?>
                <?php if ($this->GetControllerRoute() != "cashier-app" && $this->GetControllerRoute() != "order-item-app"): ?>
                <div class="copy">
                    <p> &copy; 2016 Fleet Control | Desenvolvido por 
                        <a href="http://jatecdev.com.br/" target="_blank">
                            João Augusto Tecnologia e Desenvolvimento
                        </a> 
                    </p>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <?php endif; ?>
    <script src="<?php echo $this->basePath(); ?>/js/jquery.nicescroll.js"></script>
    <script src="<?php echo $this->basePath(); ?>/js/scripts.js"></script>
    <script src="<?php echo $this->basePath(); ?>/js/bootstrap.min.js"> </script>
</body>
</html>

